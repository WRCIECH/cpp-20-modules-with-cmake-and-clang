# C++20 modules with CMake and Clang

Based on https://www.kitware.com/import-cmake-c20-modules/ and https://github.com/Kitware/CMake/blob/f1034acb02/Help/dev/experimental.rst

## Preparation

1. Install (upgrade) CMake to 3.26
2. Install (upgrade) Ninja to >=1.11

### Clang Installation

Download and unzip https://github.com/mathstuf/llvm-project/releases/tag/p1689r5-cmake-ci-20221215
Rename to `llvm-project`.

```
cd llvm-project
mkdir build
cd build
cmake -DLLVM_ENABLE_PROJECTS=clang -DCMAKE_BUILD_TYPE=Release -G Ninja ../llvm
cmake --build . -- -j 8
```

### Compilation

Paste the following commands (modifying paths to freshly build clang)
```
export CC=~/dev/private/test_modules/llvm-project/build/bin/clang
export CXX=~/dev/private/test_modules/llvm-project/build/bin/clang++
cmake --preset=macos
cmake --build --preset=macos
```